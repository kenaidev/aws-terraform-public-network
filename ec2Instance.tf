resource "aws_instance" "web1" {
    ami = "${var.AMI}"
    instance_type = "t2.micro"
    # VPC
    subnet_id = "${aws_subnet.kenaiSubnetPublic1.id}"
    # Security Group
    vpc_security_group_ids = ["${aws_security_group.openSSHandHTTP.id}"]
    # the Public SSH key
    key_name = "aws-kenai-dou"
}
