variable "AWS_REGION" {    
    default = "us-west-2"
}

variable "AMI" {
    default = "ami-038a0ccaaedae6406"
}

variable "PUBLIC_KEY" {    
    type        = string
        default = ""

}

variable "PRIVATE_KEY" {    
    type        = string
    default = ""

}


variable "EC2_USER" {    
    default = "kenai"
}