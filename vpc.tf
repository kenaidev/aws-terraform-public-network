
resource "aws_vpc" "kenaiVpcDou"{
    cidr_block = "10.0.0.0/16"
    enable_dns_support = "true"
    enable_dns_hostnames = "true"
    enable_classiclink = "false"
    instance_tenancy = "default"

    tags = {
      name = "kenaiVpcDou"
    }
}


resource "aws_subnet" "kenaiSubnetPublic1"{
  vpc_id = "${aws_vpc.kenaiVpcDou.id}"
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone = "${var.AWS_REGION}a"
  tags = {
    Name = "kenaiSubnetPublic1"
  }

}


