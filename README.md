# 11 - Terraform

## 03 – Integrating Terraform to CICD tools

![author](https://img.shields.io/badge/Authored%20by-Erick%20Torres-blue) 
 ![activity](https://img.shields.io/badge/Activity%3A-03%20--Integrating%20Terraform%20CI/CD-orange)  ![openSOurce](https://badges.frapsoft.com/os/v1/open-source.svg?v=103")

---

Simple automation for VPC, Public Network and ec2 instance for AWS with terraform

##### Instructions


1. Create a project in gitlab, then upload an existing terraform repository containing code for AWS or develop from scratch.
2. Once you previously tested terraform code is in the repository, create a new file named `gitlab-ci.yml`
3. Click on add button:
	- Select the branch
	- Select `gitlab-ci.yml`
	- Select `template type`
	- Look for `terraform` and select
4. Modify the pipeline stages accordingly
5. Setup env variables
	- On the left menu select Settings -> CICD -> Variables
	- Setup the AWS keys
6. Make a change to the infrastructure and push the changes to the repository.


##### Expected deliverables 

`gitlab-ci.yml` code and proof of successful stages completion from the pipeline, it can be a screenshot.